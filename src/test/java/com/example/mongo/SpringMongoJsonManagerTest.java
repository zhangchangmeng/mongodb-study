package com.example.mongo;

import com.example.mongo.application.SpringMongoJsonManager;
import com.example.mongo.domain.bill.Bill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringMongoJsonManagerTest {

    @Autowired
    private SpringMongoJsonManager springMongoJsonManager;

    @Test
    public void findByTotalRealMoneyBetweenTest() {
        List<Bill> bills = springMongoJsonManager.findByTotalRealMoneyBetween(new BigDecimal(10), new BigDecimal(20));
        System.out.println(bills);
    }

    @Test
    public void findByTotalRealMoneyBetweenPageTest() {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "totalRealMoney");
        Page<Bill> billPage = springMongoJsonManager.findByTotalRealMoneyBetween(new BigDecimal(1000), new BigDecimal(3000), pageable);
        System.out.println(billPage.getContent());
    }
}
