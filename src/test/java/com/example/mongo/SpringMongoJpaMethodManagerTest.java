package com.example.mongo;

import com.example.mongo.application.SpringMongoJpaMethodManager;
import com.example.mongo.domain.bill.Bill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringMongoJpaMethodManagerTest {

    @Autowired
    private SpringMongoJpaMethodManager springMongoJpaMethodManager;

    @Test
    public void findByIdTest() {
        Bill bill = springMongoJpaMethodManager.findById("PRXN01000000434");
        System.out.println(bill);
    }

    @Test
    public void findByXxxAfterTest() {
        List<Bill> billList = springMongoJpaMethodManager.findByXxxAfter(new BigDecimal(5000));
        System.out.println(billList);
    }

    @Test
    public void findByTotalRealMoneyBetweenTest() {
        List<Bill> bills = springMongoJpaMethodManager.findByTotalRealMoneyBetween(new BigDecimal(1000),new BigDecimal(3000));
        System.out.println("====> bills size ：" + bills.size());
    }

    @Test
    public void findByTotalRealMoneyBetweenPageTest() {
        Pageable pageable = PageRequest.of(0,10, Sort.Direction.DESC, "totalRealMoney");
        Page<Bill> billPage = springMongoJpaMethodManager.findByTotalRealMoneyBetween(new BigDecimal(1000),new BigDecimal(3000), pageable);
        System.out.println(billPage.getContent());
    }

    @Test
    public void deleteByTotalRealMoneyBetweenTest() {
        List<Bill> bills = springMongoJpaMethodManager.deleteByTotalRealMoneyBetween(new BigDecimal(1),new BigDecimal(5));
        System.out.println("====> delete bill ：" + bills);
    }

    @Test
    public void deleteBillByTotalRealMoneyBetweenTest() {
        Long deleteCount = springMongoJpaMethodManager.deleteBillByTotalRealMoneyBetween(new BigDecimal(5),new BigDecimal(10));
        System.out.println("====> deleteCount ：" + deleteCount);
    }
}
