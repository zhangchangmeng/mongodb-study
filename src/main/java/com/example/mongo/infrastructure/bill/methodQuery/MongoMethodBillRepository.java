package com.example.mongo.infrastructure.bill.methodQuery;

import com.example.mongo.domain.bill.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface MongoMethodBillRepository extends MongoRepository<Bill,String> {
    List<Bill> findByTotalAmountAfter(BigDecimal totalAmount);

    List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable);

    List<Bill> deleteByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    Long deleteBillByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);
}
