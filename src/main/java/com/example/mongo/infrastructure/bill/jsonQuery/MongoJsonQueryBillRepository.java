package com.example.mongo.infrastructure.bill.jsonQuery;

import com.example.mongo.domain.bill.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.math.BigDecimal;
import java.util.List;

/**
 * 使用MongoDB JSON Query查询
 * */

public interface MongoJsonQueryBillRepository extends MongoRepository<Bill, String> {
    @Query(value = "{'totalRealMoney':{$gt:?0,$lt:?1}}")
    List<Bill> findByTheBillTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    // fields参数：过滤结果集，结果只返回fields指定的字段。
    /*
        @Query(value = "{'totalRealMoney':{$gt:?0,$lt:?1}}",fields = "{'billCode':1,'totalRealMoney':1}")
        List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);
    */

    @Query(value = "{'totalRealMoney':{$gt:?0,$lt:?1}}")
    Page<Bill> findByTheBillTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable);
}
