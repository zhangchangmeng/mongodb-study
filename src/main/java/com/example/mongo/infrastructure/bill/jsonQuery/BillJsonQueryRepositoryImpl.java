package com.example.mongo.infrastructure.bill.jsonQuery;

import com.example.mongo.domain.bill.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class BillJsonQueryRepositoryImpl implements BillJsonQueryRepository {

    @Autowired
    private MongoJsonQueryBillRepository mongoJsonQueryBillRepository;

    @Override
    public List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return mongoJsonQueryBillRepository.findByTheBillTotalRealMoneyBetween(minMoney,maxMoney);
    }

    @Override
    public Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable) {
        return mongoJsonQueryBillRepository.findByTheBillTotalRealMoneyBetween(minMoney, maxMoney, pageable);
    }
}
