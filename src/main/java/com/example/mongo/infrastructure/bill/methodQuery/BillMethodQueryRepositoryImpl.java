package com.example.mongo.infrastructure.bill.methodQuery;

import com.example.mongo.domain.bill.Bill;
import com.example.mongo.infrastructure.bill.jsonQuery.MongoJsonQueryBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public class BillMethodQueryRepositoryImpl implements BillMethodQueryRepository {

    @Autowired
    private MongoMethodBillRepository mongoMethodBillRepository;

    @Autowired
    private MongoJsonQueryBillRepository mongoJsonQueryBillRepository;

    @Override
    public Optional<Bill> findById(String id) {
        return mongoMethodBillRepository.findById(id);
    }

    @Override
    public List<Bill> findByTotalAmountAfter(BigDecimal totalAmount) {
        return mongoMethodBillRepository.findByTotalAmountAfter(totalAmount);
    }

    @Override
    public List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return mongoMethodBillRepository.findByTotalRealMoneyBetween(minMoney, maxMoney);
    }

    @Override
    public Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable) {
        return mongoMethodBillRepository.findByTotalRealMoneyBetween(minMoney, maxMoney, pageable);
    }

    @Override
    public List<Bill> deleteByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return mongoMethodBillRepository.deleteByTotalRealMoneyBetween(minMoney, maxMoney);
    }

    @Override
    public Long deleteBillByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return mongoMethodBillRepository.deleteBillByTotalRealMoneyBetween(minMoney, maxMoney);
    }
}
