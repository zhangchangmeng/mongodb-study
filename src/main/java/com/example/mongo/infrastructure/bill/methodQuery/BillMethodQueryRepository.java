package com.example.mongo.infrastructure.bill.methodQuery;

import com.example.mongo.domain.bill.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface BillMethodQueryRepository {
    Optional<Bill> findById(String id);

    List<Bill> findByTotalAmountAfter(BigDecimal totalAmount);

    List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable);

    List<Bill> deleteByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    Long deleteBillByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);
}
