package com.example.mongo.infrastructure.bill.jsonQuery;

import com.example.mongo.domain.bill.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface BillJsonQueryRepository {
    List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney);

    Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable);
}
