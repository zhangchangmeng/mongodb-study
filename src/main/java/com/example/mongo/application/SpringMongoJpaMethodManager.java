package com.example.mongo.application;

import com.example.mongo.domain.bill.Bill;
import com.example.mongo.infrastructure.bill.methodQuery.BillMethodQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * 使用Spring Data Mongodb JPA对mongodb文档操作
 *
 * 1.继承Spring Data JPA
 * 2.使用方法名的形式操作Mongodb文档
 * */

@Service
public class SpringMongoJpaMethodManager {

    @Autowired
    private BillMethodQueryRepository billMethodQueryRepository;

    /**
     * 查询：findBy系列
     * */

    // findById
    public Bill findById(String id) {
        Optional<Bill> bill = billMethodQueryRepository.findById(id);
        return bill.orElse(null);
    }

    // After
    public List<Bill> findByXxxAfter(BigDecimal totalAmount) {
        return billMethodQueryRepository.findByTotalAmountAfter(totalAmount);
    }

    // Between
    public List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return billMethodQueryRepository.findByTotalRealMoneyBetween(minMoney, maxMoney);
    }

    /**
     * 分页查询
     * */
    public Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable) {
        return billMethodQueryRepository.findByTotalRealMoneyBetween(minMoney, maxMoney, pageable);
    }


    /**
     * 删除：deleteBy系列，可以使用与findBy一样的Keywords
     * */
    // List deleteByXxxBetween：返回被删除的文档记录
    public List<Bill> deleteByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return billMethodQueryRepository.deleteByTotalRealMoneyBetween(minMoney, maxMoney);
    }

    // Long deleteByXxxBetween：返回被删除文档的记录数量
    public Long deleteBillByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return billMethodQueryRepository.deleteBillByTotalRealMoneyBetween(minMoney, maxMoney);
    }
}
