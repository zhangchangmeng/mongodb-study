package com.example.mongo.application;

import com.example.mongo.domain.bill.Bill;
import com.example.mongo.infrastructure.bill.jsonQuery.BillJsonQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class SpringMongoJsonManager {

    @Autowired
    private BillJsonQueryRepository billJsonQueryRepository;

    public List<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney) {
        return billJsonQueryRepository.findByTotalRealMoneyBetween(minMoney,maxMoney);
    }

    public Page<Bill> findByTotalRealMoneyBetween(BigDecimal minMoney, BigDecimal maxMoney, Pageable pageable) {
        return billJsonQueryRepository.findByTotalRealMoneyBetween(minMoney, maxMoney, pageable);
    }
}
