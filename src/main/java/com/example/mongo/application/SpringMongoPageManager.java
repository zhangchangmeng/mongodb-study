package com.example.mongo.application;

import com.example.mongo.domain.bill.Bill;
import com.example.mongo.infrastructure.bill.methodQuery.BillMethodQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Mongo分页查询
 * */

@Service
public class SpringMongoPageManager {

    @Autowired
    private BillMethodQueryRepository billMethodQueryRepository;

    /**
     * 分页查询
     * */
    /*public Page<Bill> findPage(Pageable pageable) {
        return billMethodQueryRepository.findPage(pageable);
    }

    public Page<Bill> findPage(Predicate predicate, Pageable pageable) {
        return billMethodQueryRepository.findBillPage(predicate,pageable);
    }*/

}
