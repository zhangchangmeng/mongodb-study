package com.example.mongo.domain.operator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OperationLog {

    /**
     * 操作人
     */
    private AbstractOperator operator;

    /**
     * 操作类型
     */
    private OperationType operationType;

    /**
     * 操作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date operationTime;

    /**
     * 操作内容
     */
    private String operationContent;

    /**
     * 备注
     */
    private String memo;


    public AbstractOperator getOperator() {
        return operator;
    }

    public void setOperator(AbstractOperator operator) {
        this.operator = operator;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public String getOperationContent() {
        return operationContent;
    }

    public void setOperationContent(String operationContent) {
        this.operationContent = operationContent;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public String toString() {
        return "OperationLog{" +
                "operator=" + operator +
                ", operationType=" + operationType +
                ", operationTime=" + operationTime +
                ", operationContent='" + operationContent + '\'' +
                ", memo='" + memo + '\'' +
                '}';
    }
}

