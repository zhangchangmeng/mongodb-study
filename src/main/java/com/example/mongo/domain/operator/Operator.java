package com.example.mongo.domain.operator;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Operator extends AbstractOperator {

    /**
     * 操作人
     */
    private String operatorName;

    public Operator() {

    }

    public Operator(String operatorCode) {
        super(operatorCode);
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @Override
    public String toString() {
        return "Operator{" +
                "operatorName='" + operatorName + '\'' +
                "} " + super.toString();
    }
}

