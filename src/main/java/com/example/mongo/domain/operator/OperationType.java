package com.example.mongo.domain.operator;

public enum OperationType {
    /**
     * 创建
     */
    CREATE,
    /**
     * 审核失败
     */
    AUDIT_FAILURE,
    /**
     * 审核成功
     */
    AUDIT_SUCCESS,

    /**
     * 出库
     */
    OUT_STORAGE,

    /**
     * 入库
     */
    IN_STORAGE,
}
