package com.example.mongo.domain.operator;

import org.springframework.data.mongodb.core.index.Indexed;

public  class AbstractOperator {

    /**
     * 操作人代码
     */
    @Indexed
    private String operatorCode;

    public AbstractOperator() {
    }

    public AbstractOperator(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }
}
