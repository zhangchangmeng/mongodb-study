package com.example.mongo.domain.bill.enums;

public enum BillTypeEnum {
    /**
     * 无计划
     */
    NO_PLAN,
    /**
     * 计划
     */
    PLAN,
    /**
     * 进货
     */
    PURCHASE,
    /**
     * 配送
     */
    DELIVERY,
    /**
     * 调剂
     */
    ADJUST,
    /**
     * 退库
     */
    RESTOCK,
    /**
     * 退货
     */
    RETURNED,

    /**
     * 调拨
     */
    ALLOT,
    /**
     * 流转误差
     */
    MISTAKE,

    /**
     * 其它出入库
     */
    IN_OUT_SELF;

}
