package com.example.mongo.domain.bill;

import com.example.mongo.domain.AbstractGoods;
import com.example.mongo.domain.AbstractLocation;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

/**
 * Created by heyong on 2018/8/24 17:47
 * Description:
 *
 * @author heyong
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BillDetail {

    /**
     * 物品
     */
    private AbstractGoods goods;

    /**
     * 包
     */
    private AbstractLocation packetLocation;


    /**
     * 应拣总量
     */
    private BigDecimal shippedTotalAmount;


    /**
     * 实拣总量
     */
    private BigDecimal actualTotalAmount;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 折扣
     */
    private BigDecimal discount;

    public AbstractGoods getGoods() {
        return goods;
    }

    public void setGoods(AbstractGoods goods) {
        this.goods = goods;
    }

    public AbstractLocation getPacketLocation() {
        return packetLocation;
    }

    public void setPacketLocation(AbstractLocation packetLocation) {
        this.packetLocation = packetLocation;
    }

    public BigDecimal getShippedTotalAmount() {
        return shippedTotalAmount;
    }

    public void setShippedTotalAmount(BigDecimal shippedTotalAmount) {
        this.shippedTotalAmount = shippedTotalAmount;
    }

    public BigDecimal getActualTotalAmount() {
        return actualTotalAmount;
    }

    public void setActualTotalAmount(BigDecimal actualTotalAmount) {
        this.actualTotalAmount = actualTotalAmount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "BillDetail{" +
                "goods=" + goods +
                ", packetLocation=" + packetLocation +
                ", shippedTotalAmount=" + shippedTotalAmount +
                ", actualTotalAmount=" + actualTotalAmount +
                '}';
    }
}

