package com.example.mongo.domain.bill.enums;

public enum BillPurposeEnum {
    /**
     * 出库
     */
    OUT_STORAGE,
    /**
     * 入库
     */
    IN_STORAGE,

    /**
     * 移库
     */
    MOVE_STORAGE
}
