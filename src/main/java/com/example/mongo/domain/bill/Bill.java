package com.example.mongo.domain.bill;

import com.example.mongo.domain.*;
import com.example.mongo.domain.bill.enums.BillPurposeEnum;
import com.example.mongo.domain.bill.enums.BillTypeEnum;
import com.example.mongo.domain.operator.OperationLog;
import com.example.mongo.domain.operator.Operator;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author : zhangmeng
 * Date : 2018/10/12 16:26
 * Version : 1.0
 * Description :
 */
@Document
public class Bill extends BaseEntity {
    /**
     * 单据号
     */
    @Id
    private String billCode;

    /**
     * 单据种类
     */
    private BillTypeEnum billType;

    /**
     * 入库位置
     */
    private AbstractLocation inLocation;

    /**
     * 出库位置
     */
    private AbstractLocation outLocation;


    /**
     * 单据作用
     */
    private BillPurposeEnum billPurpose;

    /**
     * 源单号
     */
    @Indexed
    private String sourceCode;

    /**
     * 操作人
     */
    private Operator operator;

    /**
     * 数量
     */
    private BigDecimal totalAmount;

    /**
     * 品种数
     */
    private BigDecimal totalVarietyAmount;

    /**
     * 码洋
     */
    private BigDecimal totalMoney;

    /**
     * 实洋
     */
    private BigDecimal totalRealMoney;


    /**
     * 出库时间
     */
    private Date stockDate;


    /**
     * 备注
     */
    private String memo;

    /**
     * 同步来源
     */
    private SyncFrom syncFrom;


    /**
     * 单据明细
     */
    private List<BillDetail> billDetailList;

    /**
     * 操作日志
     */
    private List<OperationLog> operationLogList;

    @Transient
    private String syncStationCode;

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public BillTypeEnum getBillType() {
        return billType;
    }

    public void setBillType(BillTypeEnum billType) {
        this.billType = billType;
    }

    public AbstractLocation getInLocation() {
        return inLocation;
    }

    public void setInLocation(AbstractLocation inLocation) {
        this.inLocation = inLocation;
    }

    public AbstractLocation getOutLocation() {
        return outLocation;
    }

    public void setOutLocation(AbstractLocation outLocation) {
        this.outLocation = outLocation;
    }

    public BillPurposeEnum getBillPurpose() {
        return billPurpose;
    }

    public void setBillPurpose(BillPurposeEnum billPurpose) {
        this.billPurpose = billPurpose;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalVarietyAmount() {
        return totalVarietyAmount;
    }

    public void setTotalVarietyAmount(BigDecimal totalVarietyAmount) {
        this.totalVarietyAmount = totalVarietyAmount;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getTotalRealMoney() {
        return totalRealMoney;
    }

    public void setTotalRealMoney(BigDecimal totalRealMoney) {
        this.totalRealMoney = totalRealMoney;
    }

    public Date getStockDate() {
        return stockDate;
    }

    public void setStockDate(Date stockDate) {
        this.stockDate = stockDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<BillDetail> getBillDetailList() {
        return billDetailList;
    }

    public void setBillDetailList(List<BillDetail> billDetailList) {
        this.billDetailList = billDetailList;
    }

    public List<OperationLog> getOperationLogList() {
        return operationLogList;
    }

    public void setOperationLogList(List<OperationLog> operationLogList) {
        this.operationLogList = operationLogList;
    }

    public String getSyncStationCode() {
        return syncStationCode;
    }

    public void setSyncStationCode(String syncStationCode) {
        this.syncStationCode = syncStationCode;
    }

    public SyncFrom getSyncFrom() {
        return syncFrom;
    }

    public void setSyncFrom(SyncFrom syncFrom) {
        this.syncFrom = syncFrom;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billCode='" + billCode + '\'' +
                ", billType=" + billType +
                ", inLocation=" + inLocation +
                ", outLocation=" + outLocation +
                ", billPurpose=" + billPurpose +
                ", sourceCode='" + sourceCode + '\'' +
                ", operator=" + operator +
                ", totalAmount=" + totalAmount +
                ", totalVarietyAmount=" + totalVarietyAmount +
                ", totalMoney=" + totalMoney +
                ", totalRealMoney=" + totalRealMoney +
                ", stockDate=" + stockDate +
                ", memo='" + memo + '\'' +
                ", billDetailList=" + billDetailList +
                ", operationLogList=" + operationLogList +
                ", syncStationCode='" + syncStationCode + '\'' +
                '}';
    }

}

