package com.example.mongo.domain;

public enum SyncFrom {
    /**
     * 从益华同步
     */
    FROM_YH,
    /**
     * 从书店ERP同步
     */
    FROM_BOOK_ERP,
    /**
     * 从架存同步
     */
    FROM_SHELF,
}
