package com.example.mongo.domain;

import org.springframework.data.mongodb.core.index.Indexed;

public  class AbstractGoods {

    /**
     * 货物代码
     */
    @Indexed
    private String goodsCode;

    /**
     * 货物名称
     */
    private String goodsName;

    /**
     * 条码
     */
    @Indexed
    private String barCode;

    public AbstractGoods() {
    }

    public AbstractGoods(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public String toString() {
        return "AbstractGoods{" +
                "goodsCode='" + goodsCode + '\'' +
                '}';
    }
}
