package com.example.mongo.domain;

import org.springframework.data.mongodb.core.index.Indexed;

public class AbstractLocation {

    /**
     * 位置代码
     */
    @Indexed
    private String locationCode;

    /**
     * 位置名称
     */
    private String locationName;

    /**
     * 子地址
     */
    private AbstractLocation nextLocation;

    public AbstractLocation getNextLocation() {
        return nextLocation;
    }

    public void setNextLocation(AbstractLocation nextLocation) {
        this.nextLocation = nextLocation;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public AbstractLocation() {
    }

    public AbstractLocation(String locationCode) {
        this.locationCode = locationCode;
    }

    @Override
    public String toString() {
        return "AbstractLocation{" +
                "locationCode='" + locationCode + '\'' +
                ", locationName='" + locationName + '\'' +
                ", nextLocation=" + nextLocation +
                '}';
    }
}
